<?php
class admin extends flosso{
    function login(){
        if(isset($_GET['dashboard'])){
            $order_list = $this->get_order_list();
            require_once('../view/admin/dashboard.php');
        } else{
            require_once('../view/admin/login.php');
        } 
    }
    function get_order_list(){
        $select = "SELECT `id`, `name`, `mobile`, `email`, `address`, `no_of_tshirts`, `packing`, `total_packed`, `created_on` FROM `orders` ORDER BY created_on DESC";
        $result = $this->db_con->query($select);
        return  $result;
    }
}
?>