<?php
class order extends flosso{
    function get_order(){
        
        if(isset($_POST['order-form'])){
            $this->post_order();
        } else{
            require_once('view/user/order.php');
        }
    }
    function array_key_filter($array, $min = 0, $max = 0, $limit = 0){
        $result = array();
        $i = 0;
        foreach ($array as $key => $val) {
            if ($key >= $min && ($key <= $max || $max == 0)) {
                $result[$key] = $val;
                $i++;
            }
            if($limit>0){
                if($limit<=$i) break;
            }
        }
        return $result;
    }
    function post_order(){
        
        $inp = $this->get_post_array();
        $no_of_tshirts = @intval($inp->no_of_tshirts);
        if($no_of_tshirts >0 ){
           
            $packs_arr = $this->get_active_packs_key();
            $pack_result_first = $this->packing($packs_arr, $no_of_tshirts);
            $total_packed_first = $pack_result_first['total_packed'];
            $pack_result_final = $this->packing($packs_arr, $total_packed_first);
            $result_pack = $pack_result_final['result_pack'];
            $total_packed = $pack_result_final['total_packed'];
            if($total_packed > 0){

                $result_pack_json = json_encode(array_filter($result_pack, function($a) { return ($a !== 0); }));
                $insert = "INSERT INTO `orders`(`name`, `mobile`, `email`, `address`, `no_of_tshirts`,`packing`,`total_packed`) VALUES ('$inp->name', '$inp->mobile', '$inp->email', '$inp->address', '$no_of_tshirts', '$result_pack_json', '$total_packed')";
                $this->db_con->query($insert);
                $order_id = $this->db_con -> insert_id;
                if($order_id){
                    require_once('view/user/success.php');
                } else{
                    $error = "Packing failed 1";
                }                
            } else{
                $error = "Packing failed 2";
            }
        } else {
            $error = "Invalid count";
        }
        if($error){
            header('Location: '.BASE_URL.'?error='.$error);
        }
        
    }
    function packing($packs_arr, $order_item_count){        
        $result_pack = array();
        $total_packed = 0;

        if (array_key_exists($order_item_count, $packs_arr)){
            $item_count = $order_item_count;
            $pack_count = 1;
            $total_packed = $item_count;
            $result_pack[$item_count] = $pack_count;          
        } else {

            // Get all packs smaller than the order item count
            $result_pack = $this->array_key_filter($packs_arr, 0, $order_item_count);
            if (count($result_pack) > 0) {
                $balance = $order_item_count;
                //Array to Descending order
                krsort($result_pack);
                $item_count_min = min(array_keys($result_pack)); // Get smallest pack count
                //Packing all smaller packs in descending order
                foreach($result_pack as $item_count => $pack_count) {
                                   
                    while($item_count <= $balance){
                        $pack_count = $pack_count+1;
                        $total_packed = $total_packed+$item_count;
                        $balance = $order_item_count - $total_packed;
                        $result_pack[$item_count] = $pack_count;
                    }
                    
                    // Checking for last balance count pack
                    if($item_count_min == $item_count && $balance > 0){
                        
                        $item_count = $item_count_min;
                        $pack_count = $result_pack[$item_count_min];
                        $pack_count = $pack_count+1;
                        $total_packed = $total_packed+$item_count;
                        $balance = $order_item_count - $total_packed;
                        $result_pack[$item_count] = $pack_count;
                    }
                }                                
            }
            
            // Get the pack larger than the order item count
            $result_pack_large = $this->array_key_filter($packs_arr, $order_item_count, 0, 1);
            if (count($result_pack_large) > 0) {
                 
                $item_count = array_key_first($result_pack_large);
                if($total_packed == 0 ||  $total_packed >= $item_count){
                    // Assigning the largest pack
                    unset($result_pack);
                    $pack_count = 1;
                    $total_packed = $item_count;
                    $result_pack[$item_count] = $pack_count;
                }
            }
        }
        $result = array(
            'total_packed' => $total_packed,
            'result_pack' => $result_pack,
        );
        return $result;
    }    
}
?>