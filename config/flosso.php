<?php
class flosso{
    protected $db_name = "flosso";
    protected $db_host = "127.0.0.1";
    protected $db_user = "root";
    protected $db_password = "";
    protected $db_con = "";

    function __construct() { 

        $this->db_con = mysqli_connect($this->db_host, $this->db_user, $this->db_password, $this->db_name);
        if ($this->db_con->connect_error) {
            die("Connection failed: " . $this->db_con->connect_error);
        }
    }

    function get_post_array(){
        $input_obj = new ArrayObject;
        foreach ($_POST as $param_name => $param_val) {
            $input_name = str_replace(' ', '_', $this->clean_text($param_name));
            $input_val = $this->clean_text($param_val);
            $input_obj->$input_name = $input_val;           
        }
        return $input_obj;
    }
    function clean_text($text){
        $text = mysqli_real_escape_string($this->db_con, $text);
        $text = strip_tags($text);
        $text = stripslashes($text);
        return $text;
    }
    function get_active_packs_key(){
        $select = "SELECT  id, item_count FROM packs WHERE  status='1' ORDER BY item_count ASC";
        $result = $this->db_con->query($select);
        $packs = array();
        if ($result->num_rows > 0) {
            //Store all packs to array as key and value as 0 for update count
            while($row = $result->fetch_array()) {
                $item_count = $row['item_count'];
                $packs[$item_count] = 0;
            }
        }
        return $packs;
    }
}
?>