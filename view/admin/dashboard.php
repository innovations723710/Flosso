<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Jithin">
    <title>Flosso - Admin</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/assets/custom/css/custom.css" rel="stylesheet">  
  </head>
  <body>
    
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">Flosso</a>  
  <div class="navbar-nav">
    <div class="nav-item text-nowrap">
      <a class="nav-link px-3" href="#">Sign out</a>
    </div>
  </div>
</header>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">
              <span data-feather="home"></span>
              Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file"></span>
              Orders
            </a>
          </li>          
        </ul>

          
      </div>
    </nav>

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Orders</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
          </button>
        </div>
      </div>
  
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">#Order No</th>
              <th scope="col">Customer Name</th>
              <th scope="col">No of T-Shirts Ordered</th>
              <th scope="col">Packs</th>
              <th scope="col">Total T-Shirts Packed</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while($row = $order_list->fetch_array()) { ?>
              <tr>
                <td><?php echo $row['id'];?></td>
                <td><?php echo $row['name'];?></td>
                <td><?php echo $row['no_of_tshirts'];?></td>
                <td><?php 
                    $packing = json_decode($row['packing']);
                    foreach($packing as $item_count => $pack_count){
                      $total = $item_count*$pack_count;
                      echo "$item_count x $pack_count = $total <br>";
                    }
                ?></td>
                <td><?php echo $row['total_packed'];?></td>
              </tr><?php 
            }?>
            
          </tbody>
        </table>
      </div>
    </main>
  </div>
</div>
</body>
</html>
