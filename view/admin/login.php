<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Jithin">
    <title>Flosso - Login</title>    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/assets/custom/css/custom.css" rel="stylesheet">  
    <link href="<?php echo BASE_URL; ?>/assets/custom/css/signin.css" rel="stylesheet">      
  </head>
  <body class="text-center">
    
<main class="form-signin">
  <form>
  <h1>Flosso</h1><br><br>

    <div class="form-floating">
      <input type="email" class="form-control" id="floatingInput" value="admin" placeholder="name@example.com">
      <label for="floatingInput">Email address</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="floatingPassword" value="admin" placeholder="Password">
      <label for="floatingPassword">Password</label>
    </div>

    <div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label>
    </div>
    <a href="<?php echo BASE_URL; ?>/admin/?dashboard" class="w-100 btn btn-lg btn-primary">Sign in</a>
    <br></br>
    <p class="mt-5 mb-3 text-muted">&copy; 2023 Flosso</p>
  </form>
</main>


    
  </body>
</html>
