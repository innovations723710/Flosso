<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Jithin">
    <title>Flosso</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/checkout/">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/assets/custom/css/custom.css" rel="stylesheet">  
  </head>
  <body class="bg-light">
    
  <div class="container">
  <main>
    <div class="py-5 text-center">
      <h1>Flosso</h1>
      <br>
      <p class="lead success">Your Order has been Processed successfully!</p>
    
    </div>

    <div class="row g-5">
      <div class="col-md-2 col-lg-2"></div>
      <div class="col-md-8 col-lg-8">
        <h4 class="mb-3">Order Summary</h4>
          

            <div class="row mb-3">
              <div class="col-6 col-sm-4 themed-grid-col">No of T-shirts Ordered</div>
              <div class="col-6 col-sm-4 themed-grid-col"><?php echo $no_of_tshirts;// $inp->no_of_tshirts;?></div>
            </div>

            
            <div class="row mb-3">
            <?php //print_r($result_pack);?>
              <div class="col-6 col-sm-4 themed-grid-col">Packs will be delivered</div>
              <div class="col-6 col-sm-4 themed-grid-col">
                  <?php                       
                  $grant_total = 0;
                  foreach($result_pack as $item_count => $pack_count){
                      if($pack_count >0 ){
                        $total = $item_count*$pack_count;
                        $grant_total = $grant_total+$total;
                        echo "<div> $item_count * $pack_count = $total </div>";
                      }                          
                  }?>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-6 col-sm-4 themed-grid-col">Total T-shirts will be delivered</div>
              <div class="col-6 col-sm-4 themed-grid-col"> <?php echo $grant_total;?></div>
            </div>
            
 
 
          <hr class="my-4">

          <a href="<?php echo BASE_URL; ?>" class="w-100 btn btn-primary btn-lg">Continue Shopping</a>
        
      </div>
    </div>
  </main>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2023 Flosso</p>
     
  </footer>
</div>

    <script src="<?php echo BASE_URL; ?>/assets/dist/js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo BASE_URL; ?>/assets/custom/js/form-validation.js"></script>
  </body>
</html>
