<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Jithin">
    <title>Flosso</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/assets/custom/css/custom.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    
  <div class="container">
  <main>
    <div class="py-5 text-center">
      <h1>Flosso</h1>
      <p class="lead">Below is the order form for guest users. Regular customers should login to manage the orders.</p>
    </div>

    <div class="row g-5">
      <div class="col-md-2 col-lg-2"></div>
      <div class="col-md-8 col-lg-8">
        <h4 class="mb-3">T-Shirt Order Form</h4>
        <form method="post" action="<?php echo BASE_URL; ?>/?submit-order" class="needs-validation" novalidate>
          <div class="row g-3">
            <div class="col-sm-12">
              <label for="firstName" class="form-label">Customer name</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="" value="Jithin KR" required>
              <div class="invalid-feedback">
                Valid customer name is required.
              </div>
            </div>

            <div class="col-sm-12">
              <label for="lastName" class="form-label">Mobile</label>
              <input type="text" class="form-control" id="mobile" name="mobile" placeholder="" value="9846882326" required>
              <div class="invalid-feedback">
                Valid mobile number is required.
              </div>
            </div> 

            <div class="col-12">
              <label for="email" class="form-label">Email </label>
              <input type="email" class="form-control" id="email" name="email" value="jithinkr4@gmail.com" placeholder="you@example.com">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="col-12">
              <label for="address" class="form-label">Shipping Address</label>
              <textarea type="text" class="form-control" id="address" name="address" placeholder="#1234 Bangalore" required>Koramangala, Bangalore, Karnataka</textarea>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div> 
            
            <div class="col-sm-12">
              <label for="lastName" class="form-label">No of T-Shirts</label>
              <input type="number" class="form-control" id="no_of_tshirts" name="no_of_tshirts" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid number is required.
              </div>
            </div> 

          </div>
 
          <hr class="my-4">

          <button class="w-100 btn btn-primary btn-lg" name="order-form" type="submit">Submit</button>
        </form>
      </div>
    </div>
  </main>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2023 Flosso</p>
     
  </footer>
</div>

    <script src="<?php echo BASE_URL; ?>/assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/assets/custom/js/form-validation.js"></script>
  </body>
</html>
